## WunderFleet

### Language & tools

- PHP (>=7.0)
- MySQL (for storage) 
- Composer (for installing composer dependencies)
- npm (for installing npm dependencies)
- Frontend (bootstrap, react, redux, webpack)

##### Docs directory: /docs
##### Log directory: /storage

### Installation
This is a dockerized application. Do the following

Make sure: 
* `docker` & `docker-compose` installed in your PC.

To do:

- `cd project_directory/` into the project root directory.
- modify database config from ./config/database.php (no need for current docker environment)
- `sh ./start.sh`
- import database schema 'schema.sql' from docs directory
- modify `SIMPLETEST_BASE_URL` variable in phpunit.xml (no need for current docker environment)
- Run `docker-compose exec php vendor/bin/phpunit` 
 
 - Base url `http://127.0.0.1`.
 - API Base url `http://127.0.0.1/api`.
 - phpMyAdmin `http://127.0.0.1:8001`.
 - user: root
 - password: secret

#### Without docker
- `cd project_directory/` into the project root directory.
- modify database config from ./config/database.php
- `composer install` for installing dependencies
- optional `npm install` for installing npm dependencies (no need as I've already complied and pushed js, css)
- optional `npm run prod` for webpack build (no need as I've already complied and pushed js, css)
- create database `wunderfleet` and import database schema 'schema.sql' from docs directory
- modify `SIMPLETEST_BASE_URL` variable in phpunit.xml 
- Run `vendor/bin/phpunit` 
 - Base url `http://host`.
 - API Base url `http://host/api`.
 
### CheckList
 
 - [x] Registration API
 - [x] User availibity check API
 - [x] User registration
 - [x] Error handeling
 - [x] Logger
 - [x] Frontend
 - [x] Readme
 - [x] Unit test

## Possible performance optimizations in Code

SOLID principles & design patterns are followed for this. I think following things could be optimized:

- PHP7 features, like return type decleration. I've used strict types in many cases.
- Dependency injection. I've just created instance of a class and called its method.
- User controller's register method is quite fat. this could be less fat.
- error response could be haldled by exception handeling
- In frontend ES6 features could be used fully.




## Things could be done better

I've followed MVC & Repository pattern for API and React-Redux component pattern for frontend. I think, following things could be done better:

- I could make a seperate validator class and inject to user controller. 
- Though I've used strict typed in many cases and it could even better if I used return typed
- Repositories could be injected by following dependency injection
- error handleing could be better
- db transaction could be used in terms of failed payment
- PHPUnit test cases (did a few test)
- In frontend I could make validation as seperate component


I've used React-Redux component pattern for frontend because, feature base component can be created and those are reusable. Also other cool features including data binding, reusability, JSX.

