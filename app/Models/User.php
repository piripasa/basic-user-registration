<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";

    protected $fillable = [
        'first_name', 
        'last_name', 
        'telephone', 
        'street_address', 
        'house_number', 
        'zip_code', 
        'city',
        'account_owner',
        'iban',
        'payment_status',
        'payment_id'
    ];
    
}