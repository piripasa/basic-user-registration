<?php
namespace App\Controllers;

class FrontController extends BaseController
{
    public function home()
    {
        return $this->renderView('app');
    }
    
}