<?php
namespace App\Controllers;

use App\Lib\FleetPayment;
use App\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Validation;

class UserController extends BaseController
{
    protected $repository;
    
    public function __construct()
    {
        $this->repository = new UserRepository();
    }
    
    public function registration()
    {
        $request = Request::createFromGlobals();
        $input = $request->request->all();
        $validator = Validation::createValidator();
        $errors = [];
        $groups = new GroupSequence(['Default', 'custom']);
        $constraint = new Collection([
            'first_name' => [new Required(), new NotBlank()],
            'last_name' => [new Required(), new NotBlank()],
            'telephone' => [new Required(), new NotBlank()],
            'street_address' => [new Required(), new NotBlank()],
            'house_number' => [new Required(), new NotBlank()],
            'zip_code' => [new Required(), new NotBlank()],
            'city' => [new Required(), new NotBlank()],
            'account_owner' => [new Required(), new NotBlank()],
            'iban' => [new Required(), new NotBlank()]
        ]);
        $violations = $validator->validate($input, $constraint, $groups);
        if (0 !== count($violations)) {
            foreach ($violations as $violation) {
                $key = preg_replace('/[\[\]\.]/s', '', $violation->getPropertyPath());
                $errors[$key] = $violation->getMessage();
            }
            return $this->respondError(['message' => 'Data error', 'data' => array_map(function($k) {return $k;}, $errors)], 422);
        }
        
        //var_dump($request);
        //exit;

        if ($this->repository->isExists('telephone', $input['telephone'])) {
            return $this->respondError([
                'message' => 'Data error', 
                'data' => [
                    'telephone' => 'Telephone already taken'
                ]
            ], 422);
        }

        $user = $this->repository->saveEntity($input);

        try  {
            
            // make payment
            $data = [
                'customerId' => $user->id,
                'iban' => $user->iban,
                'owner' => $user->account_owner
            ];

            $paymentResponse = (new FleetPayment(PAYMENT_URL))->makePayment($data);

            $paymentId = $paymentResponse->paymentDataId;

            if (!$paymentId) {
                $this->repository->deleteEntity($user->id);
                throw new \Exception(json_encode($paymentResponse));
            }

            //save payment status
            $this->repository->updateEntity($user->id, [
                'payment_status' => 'SUCCESS',
                'payment_id' => $paymentId
            ]);
            
            return $this->respondSuccess([
                'message' => 'Registered Successfully', 
                'data' => [
                    'payment_id' => $paymentId
                ]
            ]);
            
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function checkUnique()
    {
        $request = Request::createFromGlobals();
        $input = $request->request->all();
        
        try  {
            if ($this->repository->isExists('telephone', $input['telephone'])) {
                return $this->respondError([
                    'message' => 'Data error', 
                    'data' => [
                        'telephone' => 'Telephone already taken'
                    ]
                    
                ], 422);
            }

            return $this->respondSuccess(['message' => 'Available']);
        } catch (\Exception $e) {
            throw $e;
        }
    }
    
}