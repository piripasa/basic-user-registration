<?php
namespace App\Controllers;

class BaseController
{
    public function respondSuccess ($data, $statusCode = 200)
    {
        $response = [];
        if (array_key_exists('message', $data)) {
            $response['message'] = $data['message'];
        }
        if (array_key_exists('data', $data)) {
            $response['data'] = $data['data'];
        }
        return $this->respond($response, $statusCode);
    }

    public function respondError ($data, $statusCode = 400)
    {
        $response['error'] = true;
        if (array_key_exists('message', $data)) {
            $response['message'] = $data['message'];
        }
        if (array_key_exists('data', $data)) {
            $response['causes'] = $data['data'];
        }
        return $this->respond($response, $statusCode);
    }

    private function respond ($data, $statusCode) {
        header("Content-Type: application/json; charset=UTF-8");
        http_response_code($statusCode);
        return json_encode($data);
    }

    public function renderView ($view, $data = []) {
        header("Content-Type: text/html; charset=UTF-8");
        $template = \App\Lib\TemplateFactory::build();
        return $template->render($view . '.html', $data);
    }
}