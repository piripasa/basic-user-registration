<?php

namespace App\Lib;

use GuzzleHttp\Client;

class ApiHandler
{
    public $uri;

    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param $endpoint
     * @return null|string
     */
    public function buildUrl(string $endpoint) {

        $apiUrl = $this->getUri();

        // Ensure base url has trailing slash
        $apiUrl .= !preg_match('#\/$#', $apiUrl) ? '/' : '';

        // Strip leading slash from endpoint
        if (preg_match('#^\/#', trim($endpoint))) {
            $endpoint = preg_replace('#^\/#', '', $endpoint);
        }

        return $apiUrl . $endpoint;
    }

    /**
     * Call api
     * @param $method
     * @param $endpoint
     * @param array $options
     * @return mixed|null
     */
    private function callApi(string $method, string $endpoint, array $options = []) {

        try {

            $client = new Client();
            $response = $client->request($method, $this->buildUrl($endpoint), $options);

            return \GuzzleHttp\json_decode($response->getBody());
        } catch (\Exception $e) {
            $logger = new LogFactory();
            $logger->error("Api request exception trying to reach $endpoint", [
                'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine()
            ]);

            return null;
        }
    }

    /**
     * @param $endpoint
     * @param array $params
     * @return mixed
     */
    public function getFromApi(string $endpoint, array $params = []) {
        return $this->callApi('GET', $endpoint, ['query' => $params]);
    }

    /**
     * @param $endpoint
     * @param array $postData
     * @return mixed
     */
    public function postToApi(string $endpoint, array $postData = []) {
        return $this->callApi('POST', $endpoint, ['json' => $postData]);
    }

    /**
     * @param $endpoint
     * @param array $putData
     * @return mixed|null
     */
    public function putToApi(string $endpoint, array $putData = []) {
        return $this->callApi('PUT', $endpoint, ['json' => $putData]);
    }
}
