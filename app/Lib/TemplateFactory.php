<?php
namespace App\Lib;

class TemplateFactory 
{
 
    public function __construct() 
    {
        //var_dump();
    }
 
    public static function build() 
    {
        // Specify Twig templates location
        $loader = new \Twig_Loader_Filesystem(__DIR__.'/../Views');
        // Instantiate Twig
        return  new \Twig_Environment($loader);
    }
}