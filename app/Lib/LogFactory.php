<?php
namespace App\Lib;

class LogFactory 
{
 
    public function __construct() 
    {
        //var_dump();
    }
 
    public static function build() 
    {
        // create the logger
        $logger = new \Monolog\Logger('general');
        $fileHandler = new \Monolog\Handler\StreamHandler(__DIR__.'/../../storage/app.log', \Monolog\Logger::INFO); 
        
        //$fileHanlder->setFormatter(new \Monolog\Formatter\JsonFormatter);
        return $logger->pushHandler($fileHandler);
        
        //$logger->info("INFO message"); // log message
    }
}