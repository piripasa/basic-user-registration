<?php

namespace App\Lib;

class FleetPayment extends ApiHandler
{
    public function __construct($uri)
    {
        $this->uri = $uri;
    }

    public function makePayment(array $data)
    {
        return $this->postToApi('/wunderfleet-recruiting-backend-dev-save-payment-data', $data);
    }
}
