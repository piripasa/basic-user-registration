<?php
namespace App\Repositories;

use App\Models\User;

class UserRepository implements RepositoryInterface {

    protected $model;

    public function __construct()
    {
        $this->model = new User();
    }

    public function saveEntity(array $data)
    {
        return $this->model->create($data);
    }

    public function updateEntity(int $id, array $data)
    {
        return $this->model->whereId($id)->update($data);
    }

    public function deleteEntity(int $id)
    {
        $user = $this->model->find($id);
        return $user->delete();
    }

    public function isExists(string $key, string $value)
    {
        return $this->model->where($key, $value)->exists();
    }
}