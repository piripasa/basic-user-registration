<?php
namespace App\Repositories;

interface RepositoryInterface
{
	
    public function saveEntity(array $data);

    public function updateEntity(int $id, array $data);

    public function deleteEntity(int $id);

    public function isExists(string $key, string $value);
}