# WunderFleet API

# Table of Contents <a name="toc"></a>

1. [User](#User)
	- [Registration](#Registration)
	- [Check user](#cc)
2. [Error response](#er)

## User <a name="User"></a>
### Registration <a name="Registration"></a> ([Top](#toc))
#### [http://127.0.0.1/api/user/registration](http://127.0.0.1/api/user/registration)
#### Request type - POST
##### POST Parameters
form-data |Data Type | Description | Required
--- | --- | --- | ---
first_name | string | Text content | YES
last_name | string | Text content | YES
telephone | string | Text content | YES
street_address | string | Text content | YES
house_number | string | Text content | YES
zip_code | string | Text content | YES
city | string | Text content | YES
account_owner | string | Text content | YES
iban | string | Text content | YES

```
{
    "message": "Registered Successfully"
}
```
```
{
    "error": true,
    "message": "Data error",
    "causes": {
        "first_name": "This field is missing.",
        "last_name": "This field is missing.",
        "account_owner": "This field is missing.",
        "iban": "This field is missing."
    }
}
```
```
{
    "error": true,
    "message": "Something went wrong"
}
```

### User availability check  <a name="cc"></a> ([Top](#toc))
#### [http://127.0.0.1/api/user/check](http://127.0.0.1/api/user/check)
#### Request type - POST
##### POST Parameters
form-data |Data Type | Description | Required
--- | --- | --- | ---
telephone | string | Text content | YES

```
{
    "message": "Available"
}
```
```
{
    "error": true,
    "message": "Data error",
    "causes": {
        "telephone": "Telephone already taken"
    }
}
```
```
{
    "error": true,
    "message": "Something went wrong"
}
```

### Error response <a name="er"></a> ([Top](#toc))

```
{
	 "error": true,
	 "message": "error string"
}
```