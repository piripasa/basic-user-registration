SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE users (
  id int(10) UNSIGNED NOT NULL,
  first_name varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  last_name varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  telephone varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  street_address varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  house_number varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  zip_code varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  city varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  account_owner varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  iban varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  payment_status enum('SUCCESS','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PENDING',
  payment_id varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE users
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY users_telephone_unique (telephone);


ALTER TABLE users
  MODIFY id int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
