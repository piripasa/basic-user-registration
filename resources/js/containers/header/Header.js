import React, {Component} from 'react'

class Header extends Component {

    render() {
        return (
            <header className="header">
                <h2 className="text-center">Wunder Mobility</h2>
            </header>
        )
    }
}

export default Header
