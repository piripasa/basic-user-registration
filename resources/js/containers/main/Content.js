import React, {Component} from 'react'
import RegistrationForm from '../../components/Registration'

class Content extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-8 offset-md-2">
                        <div className="panel panel-default">
                            <RegistrationForm/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Content
