import {CHANGE_KEY_VALUE, STORE_USER, CHECK_USER} from '../actions/RegisterAction'
import {fromJS} from 'immutable'


const INITIAL_STATE = fromJS(
    {
        user: {
            first_name: '',
            last_name: '',
            telephone: '',
            street_address: '',
            house_number: '',
            zip_code: '',
            city: '',
            account_owner: '',
            iban: ''
        },
        errors: {},
        currentStep: 1
    })


export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case `${STORE_USER}_SUCCESS`:
            return state.setIn(['user', 'first_name'], '')
                .setIn(['user', 'last_name'], '')
                .setIn(['user', 'telephone'], '')
                .setIn(['user', 'street_address'], '')
                .setIn(['user', 'house_number'], '')
                .setIn(['user', 'zip_code'], '')
                .setIn(['user', 'city'], '')
                .setIn(['user', 'account_owner'], '')
                .setIn(['user', 'iban'], '')
                .setIn(['errors'], fromJS({}))
                .setIn(['currentStep'], 1)

        case `${STORE_USER}_FAIL`:
            //console.log(action.error.response.data.causes)
            if (typeof action.error.response.data.causes == 'undefined') {
                return state;
            }
            return state.setIn(['errors'], fromJS(action.error.response.data.causes));

        case CHANGE_KEY_VALUE:
            //console.log(action.payload.value);
            return state.setIn(action.payload.key, action.payload.value)

        case `${CHECK_USER}_SUCCESS`:
            return state.setIn(['errors'], fromJS({}))
            
        case `${CHECK_USER}_FAIL`:
            if (typeof action.error.response.data.causes == 'undefined') {
                return state;
            }
            return state.setIn(['errors'], fromJS(action.error.response.data.causes));
        default:
            return state;

    }
}
