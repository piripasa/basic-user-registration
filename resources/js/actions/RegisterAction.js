export const CHANGE_KEY_VALUE = 'CHANGE_KEY_VALUE'
export const STORE_USER = 'STORE_USER'
export const CHECK_USER = 'CHECK_USER'

export const changeKeyValue = (key, value) => {
    return {
        type: CHANGE_KEY_VALUE,
        payload: {
            key: key,
            value: value
        }
    }
}

export const storeUser = (data) => {
    return {
        type: STORE_USER,
        payload: {
            request: {
                method: 'post',
                url: `user/registration`,
                data
            }
        }
    }
}

export const checkUser = (data) => {
    return {
        type: CHECK_USER,
        payload: {
            request: {
                method: 'post',
                url: `user/check`,
                data
            }
        }
    }
}