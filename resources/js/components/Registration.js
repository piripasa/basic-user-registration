import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {
    storeUser,
    changeKeyValue,
    checkUser
} from '../actions/RegisterAction'
import PersonalInformation from './Registration/PersonalInformation'
import AddressInformation from './Registration/AddressInformation'
import PaymentInformation from './Registration/PaymentInformation'
import Success from './Registration/Success'
import {fromJS} from 'immutable'

class RegistrationForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            paymentId: '',
            success: false
        }
        this._next = this._next.bind(this);
        this._prev = this._prev.bind(this);
    }

    componentWillMount() {
        const {user, changeKeyValue} = this.props
        user.map((value, key) => {
            //console.log(key);
            if (localStorage.getItem(key)) {
                //console.log(localStorage.getItem(key));
                changeKeyValue(['user', key], localStorage.getItem(key));
            } 
        })

        if (localStorage.getItem('currentStep')) {
            //console.log(localStorage.getItem('currentStep'));
            changeKeyValue(['currentStep'], parseInt(localStorage.getItem('currentStep')));
        }
    }
  
    handleChange(key, value) {
        const {changeKeyValue} = this.props
        changeKeyValue(key, value);
        localStorage.setItem(key[1], value);
    }
     
    handleSubmit() {
        const {storeUser, user} = this.props
        let data = new FormData();

        user.map((value, key) => {
            data.append(key, value)
        });

        storeUser(data).then((res) => {
            if (res.type == 'STORE_USER_SUCCESS') {
                //console.log(res);
                this.setState({paymentId: res.payload.data.data.payment_id, success: true})
                localStorage.clear();
            } else {
                alert('Something went wrong. Please validate input and try again!');
            }
        })
    }

    renderError(key) {
        const {errors} = this.props
        if (errors.get(key)) {
            return (
                <span className="help-block">{errors.get(key)}</span>
            )
        }
        return null
    }

    validate () {
        const {user, currentStep, changeKeyValue, errors} = this.props
        let isError = false;
        let errors2 = {}

        errors.map((value, key) => {
            //console.log(value);
            if (key === 'telephone') {
                isError = true;
                errors2[key] = value;
            } 
        })
        if(user.get('first_name').length < 1){
            isError = true;
            errors2['first_name'] = 'Firstname cannot be blank';
        }

        if(user.get('last_name').length < 1){
            isError = true;
            errors2['last_name'] = 'Lastname cannot be blank';
        }

        if(user.get('telephone').length < 1){
            isError = true;
            errors2['telephone'] = 'Telephone cannot be blank';
        }

        if(currentStep > 1){
            
            if(user.get('street_address').length < 1){
                isError = true;
                errors2['street_address'] = 'Street address cannot be blank';
            }

            if(user.get('house_number').length < 1){
                isError = true;
                errors2['house_number'] = 'House number cannot be blank';
            }

            if(user.get('zip_code').length < 1){
                isError = true;
                errors2['zip_code'] = 'Zip code cannot be blank';
            }

            if(user.get('city').length < 1){
                isError = true;
                errors2['city'] = 'City cannot be blank';
            }
        }

        if(currentStep > 2){
            if(user.get('account_owner').length < 1){
                isError = true;
                errors2['account_owner'] = 'Account owner cannot be blank';
            }

            if(user.get('iban').length < 1){
                isError = true;
                errors2['iban'] = 'IBAN cannot be blank';
            }
        }
        
        changeKeyValue(['errors'], fromJS(errors2))

        return isError;
    }
    
    _next () {
        const err = this.validate();
        
        if (!err) {
            const {currentStep, changeKeyValue} = this.props
            if (currentStep == 3) {
                this.handleSubmit();
            } else {
                let cs = currentStep >= 2? 3: parseInt(currentStep) + 1
                changeKeyValue(['currentStep'], cs)
                localStorage.setItem('currentStep', cs);
            }
            
        }
        
    }
      
    _prev () {
        const {currentStep, changeKeyValue} = this.props
        let cs = currentStep <= 1? 1: parseInt(currentStep) - 1
        changeKeyValue(['currentStep'], cs)
        localStorage.setItem('currentStep', cs);
    }

    checkExistingUser (key, value) {
        const {checkUser} = this.props
        let val = value.trim();
        if (val.length > 0) {
            let data = new FormData();

            data.append(key, val)

            checkUser(data).then((res) => {
                if (res.type == 'CHECK_USER_SUCCESS') {
                    //console.log(res);
                }
            })
        }
        
      }
  
  previousButton() {
    const {currentStep} = this.props
    //console.log(currentStep)
    if(currentStep !==1){
      return (
        <button className="btn btn-secondary" type="button" onClick={this._prev}>
        Previous
        </button>
      )
    }
    return null;
  }
  
  nextButton(){
    const {currentStep} = this.props
    if(currentStep <3){
      return (
        <button className="btn btn-primary float-right" type="button" onClick={this._next}>
        Save & Continue
        </button>        
      )
    }
    return null;
  }

  submitButton(){
    const {currentStep} = this.props
    if(currentStep == 3){
      return (
        <button className="btn btn-primary float-right" type="button" onClick={this._next}>
        Submit
        </button>        
      )
    }
    return null;
  }
    
    render() {
        const {user, currentStep, errors} = this.props      
        return (
            <React.Fragment>
                <div class="card">
                    <div class="card-header">
                        Register Fleet with Wunder Mobility
                    </div>
                    <div class="card-body">
                    {!this.state.success ? 
                        <form>
                            <PersonalInformation 
                                user={user} 
                                currentStep={currentStep} 
                                errors={errors} 
                                onChange={this.handleChange.bind(this)}
                                renderError={this.renderError.bind(this)}
                                onBlur={this.checkExistingUser.bind(this)}
                            />
                            <AddressInformation
                                user={user} 
                                currentStep={currentStep} 
                                errors={errors} 
                                onChange={this.handleChange.bind(this)}
                                renderError={this.renderError.bind(this)}
                            />
                            <PaymentInformation
                                user={user} 
                                currentStep={currentStep} 
                                errors={errors} 
                                onChange={this.handleChange.bind(this)}
                                renderError={this.renderError.bind(this)}
                            />
                            
                        </form> : <Success
                            paymentId={this.state.paymentId} 
                            success={this.state.success}
                        />}
                    </div>
                    {!this.state.success ? <div class="card-footer text-muted">
                        {this.previousButton()}
                        {this.nextButton()}
                        {this.submitButton()}
                    </div> : null}
                </div>
            </React.Fragment>
        );
    }
      
  }
  
  

  function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        storeUser,
        changeKeyValue,
        checkUser
    }, dispatch)
}

function mapStateToProps(state) {
    return {
        user: state.registerReducer.get('user'),
        errors: state.registerReducer.get('errors'),
        currentStep: state.registerReducer.get('currentStep')
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(RegistrationForm)

