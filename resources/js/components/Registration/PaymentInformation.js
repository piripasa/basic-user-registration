import React, {Component} from 'react'

class PaymentInformation extends Component {
    
    render() {
        const {user, currentStep, errors, onChange, renderError} = this.props
        if (currentStep !== 3) {
          return null
        } 
        return(
          <React.Fragment>
            <h5 class="card-title">Payment Information</h5>
            <div className={`form-group ${errors.get('account_owner') ? 'has-error' : ''}`}>
                <label>Account Owner</label>
                <input type="text" className="form-control" placeholder="Account Owner"
                    value={user.get('account_owner')}
                    onChange={e => {
                        e.preventDefault()
                        onChange(['user', 'account_owner'], e.target.value)
                    }}/>
                {renderError('account_owner')}
            </div>
            <div className={`form-group ${errors.get('iban') ? 'has-error' : ''}`}>
                <label>IBAN</label>
                <input type="text" className="form-control" placeholder="IBAN"
                    value={user.get('iban')}
                    onChange={e => {
                        e.preventDefault()
                        onChange(['user', 'iban'], e.target.value)
                    }}/>
                {renderError('iban')}
            </div>
          </React.Fragment>
        );
    }
}

export default PaymentInformation;

