import React, {Component} from 'react'

class PersonalInformation extends Component {
    
    render() {
        const {user, currentStep, errors, onChange, renderError, onBlur} = this.props
        
        if (currentStep !== 1) {
          return null
        } 
        return(
            <React.Fragment>
                <h5 class="card-title">Personal Information</h5>
                <div className={`form-group ${errors.get('first_name') ? 'has-error' : ''}`}>
                    <label>First Name</label>
                    <input type="text" className="form-control" placeholder="First Name" required
                        value={user.get('first_name')}
                        onChange={e => {
                            e.preventDefault()
                            onChange(['user', 'first_name'], e.target.value)
                        }}/>
                    {renderError('first_name')}
                </div>
                <div className={`form-group ${errors.get('last_name') ? 'has-error' : ''}`}>
                    <label>Last Name</label>
                    <input type="text" className="form-control" placeholder="Last Name"
                        value={user.get('last_name')}
                        onChange={e => {
                            e.preventDefault()
                            onChange(['user', 'last_name'], e.target.value)
                        }}/>
                    {renderError('last_name')}
                </div>
                <div className={`form-group ${errors.get('telephone') ? 'has-error' : ''}`}>
                    <label>Telephone</label>
                    <input type="tel" className="form-control" placeholder="Telephone"
                        value={user.get('telephone')}
                        onChange={e => {
                            e.preventDefault()
                            onChange(['user', 'telephone'], e.target.value)
                        }}
                        onBlur={e => {
                            e.preventDefault()
                            onBlur('telephone', e.target.value)
                        }}
                        />
                    {renderError('telephone')}
                </div>
          </React.Fragment>
        );
    }
}

export default PersonalInformation;

