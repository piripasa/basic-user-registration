import React, {Component} from 'react'

class AddressInformation extends Component {
    
    render() {
        const {user, currentStep, errors, onChange, renderError} = this.props
        if (currentStep !== 2) {
          return null
        } 
        return(
          <React.Fragment>
            <h5 class="card-title">Address Information</h5>
            <div className={`form-group ${errors.get('street_address') ? 'has-error' : ''}`}>
                <label>Street Address</label>
                <input type="text" className="form-control" placeholder="Street Address"
                    value={user.get('street_address')}
                    onChange={e => {
                        e.preventDefault()
                        onChange(['user', 'street_address'], e.target.value)
                    }}/>
                {renderError('street_address')}
            </div>
            <div className={`form-group ${errors.get('house_number') ? 'has-error' : ''}`}>
                <label>House Number</label>
                <input type="text" className="form-control" placeholder="House Number"
                    value={user.get('house_number')}
                    onChange={e => {
                        e.preventDefault()
                        onChange(['user', 'house_number'], e.target.value)
                    }}/>
                {renderError('house_number')}
            </div>
            <div className={`form-group ${errors.get('zip_code') ? 'has-error' : ''}`}>
                <label>Zip Code</label>
                <input type="text" className="form-control" placeholder="Zip Code"
                    value={user.get('zip_code')}
                    onChange={e => {
                        e.preventDefault()
                        onChange(['user', 'zip_code'], e.target.value)
                    }}/>
                {renderError('zip_code')}
            </div>
            <div className={`form-group ${errors.get('city') ? 'has-error' : ''}`}>
                <label>City</label>
                <input type="text" className="form-control" placeholder="City"
                    value={user.get('city')}
                    onChange={e => {
                        e.preventDefault()
                        onChange(['user', 'city'], e.target.value)
                    }}/>
                {renderError('city')}
            </div>
          </React.Fragment>
        );
    }
}

export default AddressInformation;

