import React, {Component} from 'react'

class Success extends Component {
    
    render() {
        const {paymentId, success} = this.props
        if (!success) {
          return null
        } 
        return(
          <React.Fragment>
            <h5 class="card-title text-center">Registration completed successfully</h5>
            <p class="card-text text-center">Payment ID: {paymentId}</p>
          </React.Fragment>
        );
    }
}

export default Success;

