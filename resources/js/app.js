import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter} from 'react-router-dom'
import App from './containers/App'

import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import axios from 'axios';
import {multiClientMiddleware} from 'redux-axios-middleware'

import reducers from './reducers'

import {BASE_URL} from './config'

const clients = {
    default: {
        client: axios.create({
            baseURL: `${BASE_URL}api/`,
            responseType: 'json'
        })
    }
}

let store = createStore(
    reducers,
        applyMiddleware(
            multiClientMiddleware(clients)
        )

)

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>, document.getElementById('app'))