#!/usr/bin/env bash

set -e
echo "Stopping services"
docker-compose down
echo "Starting services"
docker-compose up -d --force-recreate
docker-compose exec php chgrp -R www-data storage
docker-compose exec php chmod -R ug+rwx storage
echo "Database connection..."
sleep 3
until docker-compose exec mysql mysql -h mysql -uroot -psecret -D wunderfleet --silent -e "show databases;"
do
  echo "Waiting for database connection..."
  sleep 5
done
#docker-compose exec mysql mysql -h mysql -uroot -psecret wunderfleet < ./docs/schema.sql
echo "Installing composer dependencies"
docker-compose exec php composer install
echo "Installing npm dependencies"
docker-compose exec php npm install
docker-compose exec php npm run prod