<?php
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
 
try {
    // Add Route object(s) to RouteCollection object
    $routes = new RouteCollection();
    $routes->add('user_registration_api', new Route('/api/user/registration', ['_controller' => 'UserController:registration'], [], [], '', [], ['POST']));
    $routes->add('user_unique_api', new Route('/api/user/check', ['_controller' => 'UserController:checkUnique'], [], [], '', [], ['POST']));
    $routes->add('home', new Route('/', ['_controller' => 'FrontController:home'], [], [], '', [], ['GET']));

    // Init RequestContext object
    $context = new RequestContext();
    $context->fromRequest(Request::createFromGlobals());
 
    // Init UrlMatcher object
    $matcher = new UrlMatcher($routes, $context);
 
    // Find the current route
    $parameters = $matcher->match($context->getPathInfo());
 
    $param = explode(':', $parameters['_controller']);
    $controller = $param[0];
    $action = $param[1];
    $class = '\\App\\Controllers\\' . $controller;
    $obj = new $class;
    echo $obj->$action();
    //echo '<pre>';
    //print_r($parameters);
    exit;
} catch (ResourceNotFoundException $e) {
  throw $e;
}