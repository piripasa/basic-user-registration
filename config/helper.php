<?php

define('PAYMENT_URL', 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default');

function exception_handler($exception) {
    //echo "Exception:" . $exception->getMessage();

    $log = \App\Lib\LogFactory::build();
    $log->error($exception);

    echo (new \App\Controllers\BaseController())->respondError(['message' => $exception->getMessage()], 400);
}

set_exception_handler('exception_handler');