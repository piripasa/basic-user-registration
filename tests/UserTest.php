<?php

namespace Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * This is set up method for setting the primary need to tests
     */
    protected function setUp()
    {
        $this->client = new Client([
            'base_uri' => getenv('SIMPLETEST_BASE_URL'),
            'verify' => false
        ]);
    }
    
    /**
     * test invalid route
     */
    public function testInvalidRoute()
    {
        try {
            $response = $this->client->request('GET', '/invalid-route');
        } catch (ClientException $exception) {
            $this->assertTrue(true);
        }
        
    }

    /**
     * test Registration
     */
    public function testRegistrationSuccess()
    {
        $response = $this->client->request('POST', '/api/user/registration', ['form_params' => $this->getFormData()]);
        $this->assertEquals(200, $response->getStatusCode());
        
    }

    public function testRegistrationFail()
    {
        try {
            $response = $this->client->request('POST', '/api/user/registration', ['form_params' => []]);
        } catch (ClientException $exception) {
            $this->assertTrue(true);
        }
    }

    public function testUserRepository()
    {
        $user = (new \App\Repositories\UserRepository())->saveEntity($this->getFormData());

        $this->assertInternalType('int', $user->id);
    }

    private function getFormData()
    {
        return [
            'first_name' => str_random(), 
            'last_name' => str_random(),
            'telephone' => rand(1000000, 9999999),
            'street_address' => str_random(),
            'house_number' => str_random(),
            'zip_code' => rand(1000, 9999),
            'city' => str_random(),
            'account_owner' => str_random(),
            'iban' => str_random(),
        ];
    }
}